import json
import sys
import logging


FILENAME = './small_coco/instances_train2017_small.json'
REQUIRED_SECTIONS = [ "info" , "images" , "annotations", "categories" ]


class CocoAnnotions():

    def __init__(self, json_file:str, keys:list ) -> None:
        
        try:
            with open(FILENAME, 'r') as f:
                self.data = json.load(f)
        except:
            msg = "could not load json"
            print(msg)
            logging.error(msg)
            sys.exit(-1)
        
        #launch tests
        self.validation_results = [
            self.check_keys_exist(self.data, keys),
            self.check_coco_images_section()
        ]
        print(self.validation_results)

    def check_coco_images_section(self) -> bool:
        results = []
        required_img_fields = [
            'id', 
            'width', 
            'height', 
            'file_name'
            ]
            
        results = [ self.check_keys_exist(r, required_img_fields) for r in self.data['images']]
        results += [ self.check_file_name(r) for r in self.data['images']]
        results += [ self.check_file_id(r) for r in self.data['images']]
        results += [ self.check_dimensions(r) for r in self.data['images']]
        results.append(self.check_duplicate_ids())

        if False in results:
            return False
        else: 
            return True

    def check_duplicate_ids(self) -> bool:
        result = True
        ids = [im['id'] for im in self.data['images']]        
        seen = set()
        dupes = [str(x) for x in ids if x in seen or seen.add(x)]  
        
        #check for dupes
        try:
            assert len(dupes) == 0, f"Duplicate image identifiers found {dupes}"
        except AssertionError as err: 
            logging.error(err)
            print(err)
            result = False
        return result

    def check_file_id(self, file) -> bool:
        result = True
        id = file['id']
        fname = file['file_name']
        
        #check value is int
        try:
            assert type(id) == int, f'Expected annotation id of type int {id}, {fname}'
        except AssertionError as err: 
            logging.error(err)
            print(err)
            result = False
        return result

    def check_dimensions(self, file) -> bool:
        result = True
        height = file['height']
        width = file['width']
        fname = file['file_name']
        
        #check value is int
        try:
            assert type(height) == int, f'Expected image dimensions of type int {height}, {fname}'
            assert type(width) == int, f'Expected image dimensions of type int {width}, {fname}'
            assert height > 0, f'Expected image dimensions > 0 {height}, {fname}'
            assert width > 0, f'Expected image dimensions > 0 {width}, {fname}'
        except AssertionError as err: 
            logging.error(err)
            print(err)
            result = False
        return result
    

    def check_file_name(self, file) -> bool:
        result = True
        fname = file['file_name']
        
        #check value is a string
        try:
            assert type(fname) == str, f'Expected type string {fname}'
            
            #check the file extension
            ext = fname.rsplit('.',1)[-1].lower()
            assert ext in ['png','jpg','jpeg'], f'Invalid image file extension: {ext}, {fname}'
        
        except AssertionError as err: 
            logging.error(err)
            print(err)
            result = False

        
        return result

    def check_annotation_section():
        pass

    def check_categories_section():
        pass

    def check_keys_exist(self, data:dict, keys:list) -> bool:   
        result = True
        for key in keys:
            try:
                assert key in data, f'Missing key {key}'
            except AssertionError as err: 
                logging.error(err)
                print(err)
                result = False
        return result

def configure_logger():
    # set up logging to file
    logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='./validation.log',
                    filemode='w')

def verify_coco_json():
    coco = CocoAnnotions(FILENAME, REQUIRED_SECTIONS)
    


def main():
    configure_logger()
    print(FILENAME)
    logging.info(FILENAME)
    verify_coco_json()
    
  


if __name__ == '__main__':
    main()
    

